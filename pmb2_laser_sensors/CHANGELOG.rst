^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package pmb2_laser_sensors
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

0.11.3 (2017-06-01)
-------------------

0.11.2 (2017-04-25)
-------------------

0.11.1 (2017-04-22)
-------------------
* moved filter launch to base launch
* added filter to hokuyo launch file
* Contributors: Procópio Stein

0.11.0 (2017-02-28)
-------------------
* 0.10.4
* changelogs
* Contributors: Procópio Stein

0.10.4 (2017-02-28)
-------------------

0.10.3 (2017-02-24)
-------------------

0.10.2 (2017-02-23)
-------------------
* added dependency to pal_filters
* Contributors: Procópio Stein

0.10.1 (2017-02-23)
-------------------
* removed rgbd related files
* replaced dependency of pal_laser_filters to laser_filters
* normalized and updated laser files
* fix sick laser launch files
* Contributors: Jeremie Deray, Procópio Stein

0.10.0 (2016-03-15)
-------------------
* load laser model on param srv
* Contributors: Jeremie Deray

0.9.15 (2016-03-10)
-------------------

0.9.14 (2016-03-02)
-------------------
* rm usuless deps rplidar
* Contributors: Jeremie Deray

0.9.13 (2016-02-10)
-------------------
* revert sick tim561 time offset
* Contributors: Jeremie Deray

0.9.12 (2016-02-10)
-------------------
* fixed time_offset for tim 561
* Contributors: Procopio Stein

0.9.11 (2016-02-09)
-------------------
* launch the laser based on argument "laser"
* added launch of tim571 and modified 551 for driver sick_tim
* Contributors: Sergio Ramos

0.9.10 (2016-02-09)
-------------------
* update pmb2 laser pkg.xml
* uses sick_tim pkg rather than old version
* added support for sick tim561
* Contributors: Jeremie Deray

0.9.9 (2015-10-26)
------------------

0.9.8 (2015-10-01)
------------------
* change hokuyo port
* laser.launch param to choose hokuyo or sick
* rm rebujito_laser
* Contributors: Jeremie Deray

0.9.7 (2015-02-02)
------------------
* Replace ant -> pmb2
* Rename files
* Contributors: Enrique Fernandez
